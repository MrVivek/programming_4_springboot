package com.example.springbootapp2.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "doctor")
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "doctor_name")
    @NotBlank(message = "Doctor name shouldn't be empty")
    private String doctor_name;

    @Column(name = "years")
    private Integer years;
    @OneToMany
    private List<Patients> patients = new ArrayList<>();

    @ManyToMany
    private List<Registration> registrations = new ArrayList<>();
}
