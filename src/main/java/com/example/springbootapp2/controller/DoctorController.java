package com.example.springbootapp2.controller;

import com.example.springbootapp2.model.Doctor;
import com.example.springbootapp2.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class DoctorController {
    @Autowired
    DoctorRepository doctorRepository;

    @PostMapping(value = "/doctor")
    public String addDoctor(@RequestParam("doctor_name") String doctor_name,
                             @RequestParam("years") Integer years) {
        System.out.println("post REST");
        Doctor doctor = new Doctor();
        doctor.setDoctor_name(doctor_name);
        doctor.setYears(years);
        doctorRepository.save(doctor);
        return "add_cou";
    }

    @DeleteMapping(value = "/doctor")
    public String removeDoctor(@RequestParam("id") Integer id, Model model) {
        doctorRepository.deleteById(id);
        List<Doctor> all = doctorRepository.findAll();
        model.addAttribute("doctors", all);
        return "dashboard";
    }

    @GetMapping(value = "/doctor")
    public String getDoctor(HttpServletRequest request) {
        List<Doctor> all = doctorRepository.findAll();
        request.getSession().setAttribute("doctors", all);
        return "dashboard";
    }

    @PutMapping(value = "/doctor")
    public String editStudent(@RequestParam("doctor_name") String doctor_name,
                              @RequestParam("id") Integer id,
                              @RequestParam("years") Integer years) {
        System.out.println("put REST");
        Doctor doctor = new Doctor();
        doctor.setId(id);
        doctor.setDoctor_name(doctor_name);
        doctor.setYears(years);
        doctorRepository.save(doctor);
        return "redirect:/dashboard";
    }

}