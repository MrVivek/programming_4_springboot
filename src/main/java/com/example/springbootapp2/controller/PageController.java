package com.example.springbootapp2.controller;

import com.example.springbootapp2.model.Doctor;
import com.example.springbootapp2.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class PageController {
    @Autowired
    DoctorRepository doctorRepository;

    @RequestMapping("/home")
    public String home() {
        return "home";
    }

    @RequestMapping("")
    public String index() {
        return "home";
    }

    @RequestMapping("/dashboard")
    public String dashboard(Model model) {
        List<Doctor> all = doctorRepository.findAll();
        model.addAttribute("doctors", all);
        return "dashboard";
    }

    @RequestMapping("/add_cou")
    public String addDoctor() {
        return "add_cou";
    }

    @RequestMapping("/delete_doctor/{id}")
    public String deleteDoctor(@PathVariable(name = "id") Integer id) {
        doctorRepository.deleteById(id);
        return "dashboard";
    }


    @RequestMapping("/update")
    public String update() {
        return "update_cou";
    }

    @GetMapping("/update_doctor/{id}")
    public String updateDoctor(@PathVariable(name = "id") Integer doctorId, Model model) {
        model.addAttribute("doctor", doctorRepository.getById(doctorId));
        return "update_cou";
    }


}
